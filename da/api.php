<?php
require "dbaccess.php";

$day = $_GET["day"];

$bdd = bdd();
$requete = $bdd->prepare("SELECT hour 
                                FROM reservation
                                WHERE date = ?");
$requete->execute([$day]);
$result = $requete->fetchAll(PDO::FETCH_OBJ);


$resultJSON = json_encode((array)$result);

echo $resultJSON;


