<?php

function bdd(){
    $bdd = new PDO('mysql:host=localhost;dbname=e6coif', 'root', '');
    return $bdd;
}

function getDates(){

    $sql = "SELECT id_role, role from type_roles";

    $bdd = bdd();
    $requete = $bdd->prepare($sql);
    $requete->execute();
    $result = $requete->fetchAll(PDO::FETCH_OBJ);
    return $result;
}