<?php
require "dbaccess.php";

function maxChar($string, $count){

    if(strlen($string) > (int)$count){
        return false;
    }
    return true;
}

function minChar($string, $count){
    if(strlen($string) < (int)$count){
        return false;
    }
    return true;
}
function addReservation($name, $ntel, $date, $hour){

    $bdd = bdd();
    $requete = $bdd->prepare("INSERT INTO reservation(name, tel, date, hour) VALUES (?, ?, ?, ?)");
    $requete->execute([$name, $ntel, $date, $hour]);

    $_SESSION["name"] = null;
    $_SESSION["ntel"] = null;

}