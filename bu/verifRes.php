<?php
session_start();
require __DIR__."/../da/reservations.php";
$patternChar ="/^[a-z ,.'-]+$/i";


try{
    if(isset($_POST["name"]) && isset($_POST["ntel"])){

        $_SESSION["name"] = $name = $_POST["name"];
        $_SESSION["ntel"] = $ntel = $_POST["ntel"];
        $date = $_POST["date"];
        $hour = $_POST["hour"];

        if (empty($name) || empty($ntel)){
            $_SESSION["error"] = 1;
            $_SESSION["message"][] = "Erreur, vous n'avez rien rentré.";
            if(empty($name)){ $_SESSION["nameError"] = 1;}
            if(empty($ntel)){ $_SESSION["telError"] = 1;}

        }
        if (!preg_match($patternChar, $name)) {
            $_SESSION["error"] = 1;
            $_SESSION["nameError"] = 1;
            $_SESSION["message"][] = "Seulement l'alphabet est autorisé.";
        }
        if(!minChar($name, 3)){
            $_SESSION['error'] = 1;
            $_SESSION["nameError"] = 1;
            $_SESSION['message'][] = "Le Nom/Prenom doit avoir plus de 3 lettres";
        }
        if(!maxChar($name, 20)){
            $_SESSION['error'] = 1;
            $_SESSION["nameError"] = 1;
            $_SESSION['message'][] = "Le Nom/Prenom ne peut avoir plus de 20 lettres";
        }
        if(empty($date)){
            $_SESSION["error"] = 1;
            $_SESSION["message"][] = "Erreur, veuillez rentrer une date valide.";
        }
        if(isset($_SESSION["error"]) && $_SESSION["error"] === 1){
            header("location: ../index.php");
            exit();
        }

//        var_dump($date);
//        var_dump($hour);
//        var_dump($name);
//        var_dump($ntel);

        addReservation($name, $ntel, $date, $hour);

        header('location: ../../index.php?rdv=validate');
        exit();

    }
}catch(PDOException $e){

    echo $e->getMessage();
    echo $e->getLine();
    exit;
}
