<?php
    session_start();

    if(!isset($_SESSION["name"]) && !isset($_SESSION["ntel"])){
        $_SESSION["name"] = null;
        $_SESSION["ntel"] = null;
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Poppins:wght@400;600&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://kit.fontawesome.com/a1d75ae665.js" crossorigin="anonymous"></script>
    <title>E6'Coif</title>
</head>
<body>
    <header class="shadow">

    </header>
    <main>
        <div class="imgAffichage">
            <div class="contentPlacement contentPlacementLeft">
                <div class="divImg">
                    <img class="imgMain" src="ressources/img/salon2_e6coif.jpg" alt="">
                </div>
                <div id="slide" class="txtContent">
                    <div class="title">
                        <h1>Salon de coiffure pour femme E6'Coif</h1>
                        <p>Bienvenue dans notre salon, nous disposons de tout le materiel neccesaire dans un cadre
                            idilique pour prendre soin de vos cheveux. </p>
                    </div>
                    <div class="infoMap">
                        <div class="infos">
                            <p>Rue de Dampremy, 248 - 6000 Charleroi</p>
                            <p>Tél. 071/135796</p>
                            <p>Email : reservations@e6coif.be</p>
                            <p>Parking facile > Parking Rive Gauche</p>
                        </div>
                        <div class="divGoogle">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2542.7194437119088!2d4.438621615894949!3d50.40906599822676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2260aa361529f%3A0x4cb3599f3c97eb86!2sRue%20de%20Dampremy%2C%206000%20Charleroi!5e0!3m2!1sfr!2sbe!4v1632301596394!5m2!1sfr!2sbe" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentPlacement contentPlacementRight">
                <div class="txtContent">
                    <h1>Vous souhaitez faire une réservation ?</h1>
                    <?php
                    if(isset($_SESSION["error"]) && $_SESSION["error"] === 1){
                        $_SESSION["error"] = 0;
                        foreach($_SESSION["message"] as $message) echo "<p style='text-shadow: red 1px 0 5px; color: white; margin: 0;'>".$message. "</p> <br>";
                        $_SESSION["message"] = null;
                    }
                    if(isset($_GET["rdv"])){ ?>
                        <p>Merci de votre confiance !</p>
                    <?php }
                    ?>

                    <form class="col-5" action="bu/verifRes.php" method="POST" autocomplete="off">
                        <label for="fname">Nom de famille :</label>
                        <?php if(isset($_SESSION["nameError"]) && $_SESSION["nameError"] === 1){
                            $_SESSION["nameError"] = 0; ?>
                            <input class="formError" type="text" id="fname" name="name" value="<?php echo $_SESSION["name"] ?>">
                        <?php }else { ?>
                            <input type="text" id="fname" name="name" value="<?php echo $_SESSION["name"] ?>">
                        <?php }?>
                        <label for="ftel">Numéro de téléphone :</label>
                        <?php if(isset($_SESSION["telError"]) && $_SESSION["telError"] === 1){
                            $_SESSION["telError"] = 0; ?>
                            <input class="formError" type="tel" id="ftel" name="ntel"value="<?php echo $_SESSION["ntel"] ?>">
                        <?php }else { ?>
                        <input type="tel" id="ftel" name="ntel"value="<?php echo $_SESSION["ntel"] ?>">
                        <?php }?>
                        <label for="fdate">Jour du rendez-vous :</label>
<!--                        <input type="datetime-local" id="fdate" min="2021-09-21T00:00" step="1800" name="date">-->
                        <input type="date" id="fdate" onchange="getDate()" name="date">
                        <h1 id="test"></h1>
                        <label for="fheure">Heure :</label>
                        <select name="hour" id="fheure">
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                        </select>
                        <button type="submit">Réserver</button>
                    </form>
                </div>
                <div class="divImg">
                    <img class="imgMain" src="ressources/img/salon3_e6coif.jpg" alt="">
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div>
            <i class="fab fa-twitter"></i>
        </div>
        <div>
            <i class="fab fa-facebook-f"></i>
        </div>
        <div>
            <i class="fab fa-instagram"></i>
        </div>
    </footer>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        function getDate(){
            let date = document.getElementById('fdate').value;
            let params = {
                "day": date
            };
            axios.get('../da/api.php', { params }).then(reponse => {
                console.log(reponse.data);
            });
        };
        let choicesHours = document.getElementById("fheure").options;
        console.log(choicesHours);
    </script>
</body>
</html>