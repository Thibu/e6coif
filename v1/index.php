<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Poppins:wght@400;600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a1d75ae665.js" crossorigin="anonymous"></script>
    <title>E6'Coif</title>
</head>
<body>
    <header class="shadow">

    </header>
    <main>
        <div class="imgAffichage">
            <div class="contentPlacement contentPlacementLeft">
                <div class="circular--landscape img-hover-zoom img-hover-zoom--xyz">
                    <img src="ressources/img/salon2_e6coif.jpg" alt="">
                </div>
                <div class="txtContent">
                    <h1>Salon de coiffure pour femme E6'Coif</h1>
                    <p>Bienvenue dans notre salon, nous disposons de tout le materiel neccesaire dans un cadre
                        idilique pour prendre soin de vos cheveux. </p>
                    <p>Rue de Dampremy, 248 - 6000 Charleroi</p>
                    <p>Tél. 071/135796</p>
                    <p>Email : reservations@e6coif.be</p>
                    <p>Parking facile > Parking Rive Gauche</p>
                </div>
            </div>
            <div class="contentPlacement contentPlacementRight">
                <div class="txtContent">
                    <h1>Vous souhaitez faire une réservation ?</h1>
                    <p>Remplissez notre formulaire ci-dessous.</p>
                    <form action="" method="POST">
                        <label for="fname">Nom de famille :</label>
                        <input type="text" id="fname" name="name">
                        <label for="ftel">Numéro de téléphone :</label>
                        <input type="tel" id="ftel" name="tel">
                        <label for="fdate">Jour et heure du rendez-vous :</label>
                        <input type="datetime-local" id="fdate" min="2021-09-21T00:00" step="1800" name="date">
                        <button type="submit">Réserver</button>
                    </form>
                </div>
                <div class="img-hover-zoom img-hover-zoom--xyz">
                    <img src="ressources/img/salon3_e6coif.jpg" alt="">
                </div>
            </div>

        </div>
    </main>
    <footer>
        <div>
            <i class="fab fa-twitter"></i>
        </div>
        <div>
            <i class="fab fa-facebook-f"></i>
        </div>
        <div>
            <i class="fab fa-instagram"></i>
        </div>
    </footer>
    <script src="../assets/js/main.js"></script>
</body>
</html>